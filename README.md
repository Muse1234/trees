### What is this? ###

* Developed a Java application to implement a linked structure that represents a binary tree.
* UserInterface – which includes methods to create the initial tree and then manipulate it depending on the user’s selections from the menu.

### How do I get set up? ###

* Clone this repositorie, then copy the url. Go to ecplise, import a project from github using the url you have copied.
* From there you just need to run the code.
* The application is displayed using the consol/terminal



### Purpose ###

* This project allows you to expierment with a binary tree's by having options to miniplate the tree.
* (To miniplate the tree) You can find a Node, add a Node to prexsiting node, print the tree, and know the size of the tree.
* Also the sourcecode provided can help you brush up on your recursion skills.


### How it was made? ###

* There are two constructors in the Node class, one for the root node, and another for the nodes that are added to the root node.
* The tree consists of four important methods "findNode", "addChild", "size", "printTree"
* These four methods use recursion for clean and readable code purposes.
* There is a UserInterface runs the method "miniplateTree" which prints the menu.
* (Feel free to inspect the code, its full of detialed comments that explian each method).


