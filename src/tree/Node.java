package tree;

/**
* @author musedegu
* {@summary} This class is where nodes created and its methods
*/


public class Node implements INode {
	
	private String ID;
	private Node Parent;
	private Node[] Children = new Node[2];
	
	public Node(String ID) {
		this.ID = ID;
		Parent = null;
		
	}
	public Node(String newNodeID, Node parent) {
		this.ID = newNodeID;
		this.Parent = parent;
	}
	
	//======================================================================//
	/**
	* This method checks to see if a node with the parentID exists in the tree.
	* If not, it returns false after printing an appropriate message. If the
	* node exists, the method checks to see if it already has two children. If
	* it does, the method returns false. Otherwise, it either adds the new node
	* as the parent node’s left child (if the parent has no children) or as the
	* right child (if the parent already has one child). A recursive approach is
	* used.
	*
	* @param ID The ID of the new node to be added to the tree.
	* @param parentID The ID of the parent of the new node.
	* @return true if new node was added successfully, false otherwise.
	*/
	public boolean addChild(String ID, String parentID) {
		if(!getId().equals(parentID)) {
			if(Children[0] != null)
				Children[0].addChild(ID, parentID);
			if(Children[1] != null)
				Children[1].addChild(ID, parentID);
		}
		else {
			if(Children[1] != null)
				return false;
			if(Children[0] == null)
				Children[0] = new Node(ID, this);
			else
				Children[1] = new Node(ID, this);
		}
		return true;
	}
	
	//======================================================================//
	
	/**
	* This method looks within the tree to find if “value” (the ID of the node
	* to be found) is contained in that subtree. As the search progresses down
	* the tree different nodes will be calling find().
	*

	* @param value a string (ID of a node) to be found in the tree
	* @return the node, if found; null otherwise.
	*/
	public INode find(String value) {
		if (ID.equals(value)) {
			return this;
		}
		if (Children[0] == null) {
			return null;
		}
		if(Children[0] != null && Children[1] == null) {
			return Children[0].find(value);
		}
		if (Children[0] != null && Children[1] != null) {
			if (Children[0].find(value) == null) {
				return Children[1].find(value);
			}
			else {
				return Children[0].find(value);
			}
		}
		return this;
	}
	
	
	//======================================================================//
	
	public INode getParent() {
		return Parent;
	}
	
	//======================================================================//
	/**
	* Returns a string with the IDs of this node and its immediate children (if
	* any) all on one line.
	*
	* @return a string with the IDs of this node and its immediate children (if
	* any).
	*/
	public String toString() {
		if(Children[0] == null) {
			return getId();
		}
		if ((Children[0] != null) && (Children[1] == null)) {
			return getId() + Children[0].getId();
		}
		if((Children[0] != null) && (Children[1] != null)) {
			return getId() + Children[0].getId() + Children[1].getId();
		}
		return null;
	}
	
	//======================================================================//
	/**
	* Returns the ID of this node.
	*
	* @return the String ID of this node.
	*/
	public String getId() {
		return ID;
	}
	
	//======================================================================//
	/**
	* Uses recursion (and the toString() method) to print (on a line) each
	* node's ID and those of any children it has.
	*/
	public void printTree() {
		
		System.out.println(toString());
		if (Children[0] != null) {
			Children[0].printTree();
			if(Children[1] != null) {
				Children[1].printTree();
			}
		}

	}
	
	//======================================================================//
	/**
	* Returns the size of the tree or subtree whose root calls this method.
	*
	* @return the size of the tree (or subtree) starting from this node
	* all the way down to the leaf nodes.
	*/
	public int size() {
		if(Children[0] == null) {
			return 1;
		}
		if(Children[0] != null && Children[1] == null) {
			return 1 + Children[0].size();
		}
		if(Children[0] != null && Children[1] != null) {
			return 1 + Children[0].size() + Children[1].size();
		}
		return 1;
	}
}