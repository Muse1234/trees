package tree;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserInterFace {
	
	private Node root;
	private boolean repeat = true;
	private boolean repeat2 = true;
	private Scanner sc;
	/**
	 * This method create the root and fills it with nodes.
	 */
	public void createTree() {
		root = new Node("A");
		
		root.addChild("B", "A");
		root.addChild("C", "A");
		root.addChild("D", "B");
		root.addChild("E", "B");
		root.addChild("F", "C");
		root.addChild("G", "C");
		root.addChild("H", "D");
		root.addChild("I", "D");
		root.addChild("J", "E");
		root.addChild("K", "E");
		root.addChild("L", "F");
		root.printTree();
		System.out.println("There are " + root.size() + " nodes in this tree.\n");
	}
	 /**
	 * This method uses recursion to loop as long
	 *	as the user wants to, and perform the tasks desired by the user.
	 */
	public void manipulateTree() {
		if (repeat == true) {
			createTree();
		}
		if (repeat2 == true) {
			printMenu();
		}
		int choose = 0;
		try {
			sc = new Scanner(System.in);
			choose = sc.nextInt();
		}
		catch(InputMismatchException e) {
			System.out.println("Please Enter An Integer");
			repeat = false;
			repeat2 = false;
			manipulateTree();
		}
		if ((choose != 1) && (choose != 2) && (choose != 3) && (choose != 0)) {
			System.out.println("Invalid input!");
			repeat = false;
			repeat2 = true;
			manipulateTree();
		}
		if (choose == 1) {
			Scanner sc1 = new Scanner(System.in);
			String add = "";
			String parent = "";
			System.out.println("Please input the node you want to add->");
			add = sc1.nextLine();
			System.out.println("Please input the parent node of "+ add + " ->");
			parent = sc1.nextLine();
			if (root.addChild(add, parent) == false) {
				System.out.println("Parent already has 2 children.");
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
			else {
				System.out.println("Node successfully added!");
				root.printTree();
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
		}
		if (choose == 2) {
			Scanner sc2 = new Scanner(System.in);
			System.out.println("Please input the root node->");
			String root1 = "";
			root1 = sc2.nextLine();
			root.find(root1);
			if (root.find(root1) != null) {
				System.out.println("There are " + root.find(root1).size() + " nodes in this tree.\n");
				root.find(root1).printTree();
				System.out.println("");
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
			else {
				System.out.println("There are no such node " + root1 + " in this tree.\n");
				System.out.println("");
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
		}
		if (choose == 3) {
			Scanner sc3 = new Scanner(System.in);
			System.out.println("Please input the node you want to find->");
			String node = "";
			node = sc3.nextLine();
			if (root.find(node) != null) {
				System.out.println("Node "+ node + " found!");
				root.find(node).printTree();
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
			else {
				System.out.println("Node "+ node + " does not exist.");
				repeat = false;
				repeat2 = true;
				manipulateTree();
			}
		}
	}
	/**
	 * This method simply prints the menu, the user chooses from.
	 */
	public void printMenu() {
		System.out.println("Please select one of the following options:");
		System.out.println("1. Add Node");
		System.out.println("2. Tree Size");
		System.out.println("3. Find Node");
		System.out.println("0. Exit");
	}
}
